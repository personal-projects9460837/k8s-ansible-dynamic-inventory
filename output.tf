output "Worker_nodes_private_ip" {
  value = module.k8s_server.worker_ip
}

output "Master_nodes_private_ip" {
  value = module.k8s_server.master_ip
}

output "bastion_public_ip" {
  value = module.bastion_server.bastion_ip
}

output "haproxy1-servers-ip" {
  value = module.haproxy_server.haproxy_master_ip
}

output "haproxy2-servers-ip" {
  value = module.haproxy_server.haproxy_backup_ip
}

output "jenkins-servers-ip" {
  value = module.jenkins_server.jenkins_ip
}

output "ansible-servers-ip" {
  value = module.ansible_server.ansible_ip
}

output "prod_dns_name" {
  value = "https://${var.prod_record_name}"
}

output "stage_dns_name" {
  value = "https://${var.stage_record_name}"
}

output "jenkins_dns_name" {
  value = "https://${var.jenkins_record_name}"
}

output "grafana_dns_name" {
  value = "https://${var.grafana_record_name}"
}

output "prometheus_dns_name" {
  value = "https://${var.prometheus_record_name}"
}