provider "aws" {
  region     = "eu-west-2"
  profile    = "default"
}

module "vpc" {
    source           = "./modules/vpc"
    vpc-cidr         = var.vpc-cidr
    subnet-cidr-pub  = var.subnet-cidr-pub
    subnet-cidr-priv = var.subnet-cidr-priv
    pub-subnet       = var.pub-subnet
    priv-subnet      = var.priv-subnet
    route-cidr       = var.route-cidr

}

module "security_gp" {
  source  = "./modules/security-group"
  sg-cidr = var.sg-cidr
  vpc_id  = module.vpc.vpc-id
}

module "key_pair" {
  source = "./modules/keypair"
}

module "role" {
  source = "./modules/roles"
}

module "ansible_server" {
  source           = "./modules/ansible"
  ami_ubuntu       = module.key_pair.ami_ubuntu
  key_pair         = module.key_pair.pub_key
  priv_subnet1     = module.vpc.priv-sub1
  ansible_sg       = module.security_gp.ansible_sg
  priv-key         = module.key_pair.priv_key
  instance_profile = module.role.instance_profile
  haproxy1-ip      = module.haproxy_server.haproxy_master_ip
  haproxy2-ip      = module.haproxy_server.haproxy_backup_ip
  bastion-host     = module.bastion_server.bastion_ip

}

module "bastion_server" {
  source = "./modules/bastion"
  ami_ubuntu    = module.key_pair.ami_ubuntu
  key_pair      = module.key_pair.pub_key
  priv_key      = module.key_pair.priv_key
  pub_subnet1   = module.vpc.pub-sub-1
  bastion_sg    = module.security_gp.bastion_sg

}

module "k8s_server" {
  source         = "./modules/k8s-nodes"
  k8s_sg         = module.security_gp.k8s_sg
  ami_ubuntu     = module.key_pair.ami_ubuntu
  prvsub-id      = module.vpc.prvsub-id
  key_pair       = module.key_pair.pub_key
  instance-count = var.instance-count
}

module "jenkins_server" {
  source = "./modules/jenkins"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  priv_subnet1 = module.vpc.priv-sub1
  jenkins_sg = module.security_gp.jenkins_sg
}

module "haproxy_server" {
  source = "./modules/haproxy"
  ami_ubuntu = module.key_pair.ami_ubuntu
  key_pair = module.key_pair.pub_key
  priv_subnet1 = module.vpc.priv-sub1
  priv_subnet3 = module.vpc.priv-sub3
  haproxy_sg = module.security_gp.haproxy_sg
  master1-ip = module.k8s_server.master_ip[0]
  master2-ip = module.k8s_server.master_ip[1]
  master4-ip = module.k8s_server.master_ip[0]
  master5-ip = module.k8s_server.master_ip[1]

}

module "route53" {
  source = "./modules/route53"
  domain_name = var.domain_name
  domain_name2 = var.domain_name2
  jenkins_record_name = var.jenkins_record_name
  prod_record_name = var.prod_record_name
  stage_record_name = var.stage_record_name
  grafana_record_name = var.grafana_record_name
  prometheus_record_name = var.prometheus_record_name
  jenkins_lb_dns_name = module.jenkins_server_lb.jenkins_dns_name
  prod_lb_dns_name = module.prod_lb.prod_dns_name
  stage-lb-dns-name = module.stage_lb.stage_dns_name
  prometheus-lb-dns-name = module.prometheus_lb.prometheus_dns_name
  grafana-lb-dns-name = module.grafana_lb.grafana_dns_name
  jenkins_lb_zone_id = module.jenkins_server_lb.jenkins_zone_id
  prod_lb_zone_id = module.prod_lb.prod_zone_id
  stage-lb-zone-id = module.stage_lb.stage_zone_id
  grafana-lb-zone-id = module.grafana_lb.grafana_zone_id
  prometheus-lb-zone-id = module.prometheus_lb.prometheus_zone_id

}

module "grafana_lb" {
  source = "./modules/grafana-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  certificate_arn = module.route53.certificate_arn
  worker-node_id = module.k8s_server.worker_id

}

module "prometheus_lb" {
  source = "./modules/prometheus-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  certificate_arn = module.route53.certificate_arn
  worker-node_id = module.k8s_server.worker_id

}

module "jenkins_server_lb" {
  source            = "./modules/jenkins-lb"
  lb_sg             = module.security_gp.jenkins_lb_sg
  pub_subnet_1      = module.vpc.pub-sub-1
  pub_subnet_2      = module.vpc.pub-sub-2
  vpc_id            = module.vpc.vpc-id
  certificate_arn   = module.route53.certificate_arn
  jenkins_server_id = module.jenkins_server.jenkins_id

}

module "prod_lb" {
  source = "./modules/prod-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  certificate_arn = module.route53.certificate_arn
  worker-node_id = module.k8s_server.worker_id

}

module "stage_lb" {
  source = "./modules/stage-lb"
  k8s_sg = module.security_gp.k8s_sg
  pub_subnet_1 = module.vpc.pub-sub-1
  pub_subnet_2 = module.vpc.pub-sub-2
  vpc_id = module.vpc.vpc-id
  certificate_arn = module.route53.certificate_arn
  worker-node_id = module.k8s_server.worker_id

}
