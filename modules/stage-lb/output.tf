output "stage_dns_name" {
  value = aws_lb.stage_load_balancer.dns_name
}

output "stage_zone_id" {
  value = aws_lb.stage_load_balancer.zone_id
}