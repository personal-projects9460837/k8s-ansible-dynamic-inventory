output "jenkins_dns_name" {
  value = aws_lb.jenkins_load_balancer.dns_name
}
output "jenkins_zone_id" {
  value = aws_lb.jenkins_load_balancer.zone_id
}

