#!/bin/bash
sudo apt update -y
sudo add-apt-repository ppa:vbernat/haproxy-2.4 -y
sudo apt update
sudo apt install haproxy=2.4.\* -y

sudo bash -c 'echo "
frontend myfrontend
  bind 0.0.0.0:6443
  mode tcp
  option tcplog
  default_backend mybackend

backend mybackend
  mode tcp
  balance roundrobin
  option tcp-check
  server master1 ${master1-ip}:6443
  server master2 ${master2-ip}:6443" > /etc/haproxy/haproxy.cfg'    

sudo systemctl restart haproxy
