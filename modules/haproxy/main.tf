# launch master haproxy server
resource "aws_instance" "haproxy_master" {
  ami                                    = var.ami_ubuntu
  instance_type                          = "t2.micro"
  subnet_id                              = var.priv_subnet1
  vpc_security_group_ids                 = [var.haproxy_sg]
  key_name                               = var.key_pair
  user_data                              = templatefile("./modules/haproxy/haproxy.sh", {
    master1-ip        = var.master1-ip,
    master2-ip        = var.master2-ip
  
  })

  tags = {
    Name                                 = "haproxy-master"
  }
}

# launch backup haproxy server
resource "aws_instance" "haproxy_backup" {
  ami                                    = var.ami_ubuntu
  instance_type                          = "t2.micro"
  subnet_id                              = var.priv_subnet3
  vpc_security_group_ids                 = [var.haproxy_sg]
  key_name                               = var.key_pair
  user_data                              = templatefile("./modules/haproxy/haproxy_backup.sh", {
    master4-ip = var.master4-ip,
    master5-ip = var.master5-ip
    
  })

  tags = {
    Name                                 = "haproxy-backup"
  }
}