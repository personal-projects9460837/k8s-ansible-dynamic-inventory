output "haproxy_master_ip" {
  value = aws_instance.haproxy_master.private_ip
}

output "haproxy_backup_ip" {
  value = aws_instance.haproxy_backup.private_ip
}

