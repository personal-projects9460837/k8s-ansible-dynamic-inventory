resource "aws_instance" "ansible_server" {
  ami                         = var.ami_ubuntu
  instance_type               = "t2.micro"
  key_name                    = var.key_pair
  subnet_id                   = var.priv_subnet1
  iam_instance_profile        = var.instance_profile
  vpc_security_group_ids      = [ var.ansible_sg ]
  user_data                   = templatefile("./modules/ansible/ansible.sh", {
    priv-key    = var.priv-key,
    haproxy1-ip = var.haproxy1-ip,
    haproxy2-ip = var.haproxy2-ip
  })

  tags = {
    Name = "Ansible_Server"
  }
}

# create null resource to copy playbook directory into ansible server
resource "null_resource" "copy-playbook" {
  connection {
    type = "ssh"
    host = aws_instance.ansible_server.private_ip
    user = "ubuntu"
    private_key = var.priv-key
    bastion_host = var.bastion-host
    bastion_user = "ubuntu"
    bastion_host_key = var.priv-key

  }
  provisioner "file" {
    source = "./modules/ansible/playbooks"
    destination = "/home/ubuntu/playbooks"
  }

  provisioner "file" {
    source      = "./ansible.cfg"
    destination = "/home/ubuntu/ansible.cfg"
  }

  provisioner "file" {
    source      = "./aws_ec2.yaml"
    destination = "/home/ubuntu/aws_ec2.yaml"
  }

}