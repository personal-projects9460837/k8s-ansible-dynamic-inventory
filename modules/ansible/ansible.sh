#!/bin/bash
sudo hostnamectl set-hostname ansible
sudo apt update
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y
sudo apt install python3-pip -y
sudo apt install python3-boto3 -y

# Copying private key into ansible Server
echo "${priv-key}" >> /home/ubuntu/.ssh/myKey
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
sudo chmod 400 /home/ubuntu/.ssh/myKey

# Copying the 1st HAproxy into our ha-ip.yml
sudo echo haproxy_master_ip: "${haproxy1-ip}" >> /home/ubuntu/hap-ip.yaml

# Copying the 2st HAproxy into our ha-ip.yml
sudo echo haproxy_backup_ip: "${haproxy2-ip}" >> /home/ubuntu/hap-ip.yaml

