# use data source to get a registered amazon-linux 2 ami

data "aws_ami" "ubuntu" {

  most_recent                     = true

  owners                          = ["099720109477"]

  filter {
    name                         = "name"
    values                       = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name                         = "virtualization-type"
    values                       = ["hvm"]
  }

}

#creating a key pair in terraform

resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  key_name   = "myKey" # Create a "myKey" to AWS!!
  public_key = tls_private_key.pk.public_key_openssh
}

resource "local_file" "ssh_key" {
  filename = "${aws_key_pair.kp.key_name}.pem"
  content  = tls_private_key.pk.private_key_pem
}