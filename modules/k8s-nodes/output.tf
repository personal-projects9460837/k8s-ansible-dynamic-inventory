output "master_ip" {
  value = aws_instance.master-node.*.private_ip
}

output "worker_ip" {
  value = aws_instance.worker-node.*.private_ip
}

# output "worker_id" {
#   value = aws_instance.worker-node.id
# }

output "worker_id" {
  value = aws_instance.worker-node.*.id
}