output "vpc-id" {
  value = aws_vpc.k8s-vpc.id
}

output "pub-sub-1" {
  value = aws_subnet.k8s-pub1.id
}

output "pub-sub-2" {
  value = aws_subnet.k8s-pub2.id
}

output "pub-sub-3" {
  value = aws_subnet.k8s-pub3.id
}

output "priv-sub1" {
  value = aws_subnet.k8s-priv1.id
}

output "priv-sub2" {
  value = aws_subnet.k8s-priv2.id
}

output "priv-sub3" {
  value = aws_subnet.k8s-priv3.id
}

output "prvsub-id" {
  value = [aws_subnet.k8s-priv1.id, aws_subnet.k8s-priv2.id, aws_subnet.k8s-priv3.id]
}

output "pub-cidr-1" {
  value = var.subnet-cidr-pub[0]
}

output "pub-cidr-2" {
  value = var.subnet-cidr-pub[1]
}

output "pub-cidr-3" {
  value = var.subnet-cidr-pub[2]
}
output "subnet-list" {
  value = var.subnet-cidr-pub
}